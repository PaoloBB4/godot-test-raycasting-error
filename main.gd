
extends Spatial


func _ready():
	var block = get_node("block1")
	var pos = block.get_translation()
	
	var spaceState = get_world().get_direct_space_state()
	var castResult = spaceState.intersect_ray(pos, pos-Vector3(0, 10, 0))
	
	if not castResult.empty():
		print("Collision occured")
		block.set_translation(castResult["position"])
	else:
		print("No collision occured")
