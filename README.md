This is a simple test project to showcase a problem with ray-casting.

There are two objects in the scene, 'block1' (color red) and 'block2' (colored green).
'block1' is a MeshInstance while 'block2' is a StaticBody with two children: a CollisionShape and a MeshInstance.

Ray-casting is done from 'block1's position and downwards 10 units.

'block2' has the 'ray_pickable' property ticked off (set to false) yet collision happens uppon ray-casting. In fact, it persists even with no 'CollisionShape' child under 'block2'.
